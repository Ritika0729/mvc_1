﻿using Microsoft.EntityFrameworkCore;
using ProductWebApplication.MVC.Context;
using ProductWebApplication.MVC.Models;

namespace MVCUserAppTest

{
    public class UserDbFixture
    {
        internal ProductDbContext _productDbContext;
        public UserDbFixture()
        {
            var userDbContextOptions = new DbContextOptionsBuilder<ProductDbContext>().UseInMemoryDatabase("UserDb").Options;
            _productDbContext = new ProductDbContext(userDbContextOptions);
            _productDbContext.Add(new User() { Id = 1, Name = "User1 ", Password = "User1", Location = "Indore" });
            _productDbContext.SaveChanges();
        }
    }
}